<?php

require_once 'vendor/autoload.php';

use App\Service\CommissionCalculatorManager;
use App\Client\BinlistApiClient;
use App\Client\ExchangeRateApiClient;
use App\Service\ReportGenerator;

$commissionCalculator = new CommissionCalculatorManager(
    new BinlistApiClient(),
    new ExchangeRateApiClient('xxx')
);

$reportGenerator = new ReportGenerator($commissionCalculator);

$reportGenerator->generate($argv[1]);