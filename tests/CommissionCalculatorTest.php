<?php

namespace tests;

use App\Client\BinlistClientInterface;
use App\Client\ExchangeRateClientInterface;
use App\Dto\TransactionDto;
use PHPUnit\Framework\TestCase;

use App\Service\CommissionCalculatorManager;

final class CommissionCalculatorTest extends TestCase
{
    private const TRANSACTION = 't';
    private const BINLIST_RESPONSE = 'b';
    private const RATE_RESPONSE = 'r';
    private const EXPECTED = 'e';

    public function defaultFlowDataProvider(): array
    {
        return [
            'eur_without_convertation_eu_country' => [
                self::TRANSACTION => '{"bin":"45717360","amount":"100.00","currency":"EUR"}',
                self::BINLIST_RESPONSE => 'responses/binlist/bin_45717360.json',
                self::RATE_RESPONSE => 'responses/rate/mocked_response.json',
                self::EXPECTED => 1.0
            ],
            'usd_with_convertation_eu_country' => [
                self::TRANSACTION => '{"bin":"516793","amount":"50.00","currency":"USD"}',
                self::BINLIST_RESPONSE => 'responses/binlist/bin_516793.json',
                self::RATE_RESPONSE => 'responses/rate/mocked_response.json',
                self::EXPECTED => 0.5555555555555556
            ],
            'cny_with_convertation_and_not_eu_country' => [
                self::TRANSACTION => '{"bin":"4745030","amount":"2000.00","currency":"CNY"}',
                self::BINLIST_RESPONSE => 'responses/binlist/bin_4745030_custom.json',
                self::RATE_RESPONSE => 'responses/rate/mocked_response.json',
                self::EXPECTED => 4.0
            ],
            'jpy_with_invalid_bin' => [
                self::TRANSACTION => '{"bin":"45417360","amount":"10000.00","currency":"JPY"}',
                self::BINLIST_RESPONSE => 'responses/binlist/bin_45417360.json',
                self::RATE_RESPONSE => 'responses/rate/mocked_response.json',
                self::EXPECTED => null
            ],
            'usd_with_invalid_rate' => [
                self::TRANSACTION => '{"bin":"41417360","amount":"130.00","currency":"USD"}',
                self::BINLIST_RESPONSE => 'responses/binlist/bin_41417360.json',
                self::RATE_RESPONSE => 'responses/rate/invalid_request.json',
                self::EXPECTED => null
            ],
        ];
    }

    /**
     * @dataProvider defaultFlowDataProvider
     */
    public function testCommissionCalculation(
        string $transactionData,
        string $binlistResponseFile,
        string $rateResponseFile,
        ?float $expectedResult
    ) {
        $binlistApiClient = $this->createMock(BinlistClientInterface::class);

        $binlistApiClient->method('getBinInfo')
            ->willReturn(json_decode(file_get_contents($binlistResponseFile), true));

        $rateApiClient = $this->createMock(ExchangeRateClientInterface::class);

        $rateApiClient->method('getRates')
            ->willReturn(json_decode(file_get_contents($rateResponseFile), true));

        $commissionCalculator = new CommissionCalculatorManager(
            $binlistApiClient,
            $rateApiClient
        );

        if (null === $expectedResult) {
            self::expectException(\DomainException::class);
        }

        $actualResult = $commissionCalculator->calculate(
            TransactionDto::create(json_decode($transactionData, true))
        );

        if (null !== $expectedResult) {
            self::assertEquals($expectedResult, $actualResult);
        }
    }
}
