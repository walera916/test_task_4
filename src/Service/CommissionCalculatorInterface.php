<?php

namespace App\Service;

use App\Dto\TransactionDto;

interface CommissionCalculatorInterface
{
    public function calculate(TransactionDto $transactionDto): float;
}
