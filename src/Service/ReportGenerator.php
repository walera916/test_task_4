<?php

namespace App\Service;

use App\Dto\TransactionDto;

final class ReportGenerator
{
    public function __construct(
        private readonly CommissionCalculatorInterface $commissionCalculator
    ) {
    }

    public function generate(string $filePath): void
    {
        $content = file_get_contents($filePath);
        if (false === $content) {
            throw new \DomainException('Invalid file');
        }

        foreach (explode("\n", $content) as $row) {
            echo $this->commissionCalculator->calculate(TransactionDto::create(json_decode($row, true)));
            echo "\n";
        }
    }
}
