<?php

namespace App\Service;

use App\Client\BinlistClientInterface;
use App\Client\ExchangeRateClientInterface;
use App\Dto\TransactionDto;
use App\Enum\CurrencyEnum;
use App\Enum\EUCountryAlpha2Enum;

//Here I have some doubt, maybe it must be EuStrategy if we can calculate commission in different currency
//Otherwise if company take commission from all client in one currency - this be correct
final class CommissionCalculatorManager implements CommissionCalculatorInterface
{
    private const EU_COMMISSION = 0.01;
    private const NOT_EU_COMMISSION = 0.02;

    public function __construct(
        private readonly BinlistClientInterface $binlistClient,
        private readonly ExchangeRateClientInterface $exchangeRateClient
    ) {
    }

    public function calculate(TransactionDto $transactionDto): float
    {
        $binlistData = $this->binlistClient->getBinInfo($transactionDto->bin);
        if (!array_key_exists('country', $binlistData) ||
            !array_key_exists('alpha2', $binlistData['country'])) {
            throw new \DomainException('Invalid bin');
        }

        $isEuBin = EUCountryAlpha2Enum::isEU($binlistData['country']['alpha2']);
        $amountFixed = $this->calculateAmountFixed($transactionDto);

        if ($isEuBin) {
            return $amountFixed * self::EU_COMMISSION;
        }

        return $amountFixed * self::NOT_EU_COMMISSION;
    }

    private function getRate(string $currency): float
    {
        $ratesData = $this->exchangeRateClient->getRates();
        $rate = 0;

        if (\array_key_exists('rates', $ratesData)
            && \array_key_exists($currency, $ratesData['rates'])
        ) {
            $rate = $ratesData['rates'][$currency];
        }

        return $rate;
    }

    private function calculateAmountFixed(TransactionDto $transactionDto): float
    {
        $amountFixed = (float) $transactionDto->amount;
        $rate = $this->getRate($transactionDto->currency);

        if ($transactionDto->currency !== CurrencyEnum::EUR->value || $rate > 0) {
            if ($rate === 0.0) {
                throw new \DomainException(\sprintf('Incorrect rate %s', $rate));
            }

            $amountFixed = $amountFixed / $rate;
        }

        return $amountFixed;
    }
}
