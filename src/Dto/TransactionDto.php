<?php

namespace App\Dto;

final class TransactionDto
{
    public readonly string $bin;
    public readonly string $amount;
    public readonly string $currency;

    private function __construct(string $bin, string $amount, string $currency)
    {
        $this->bin = $bin;
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @param array<mixed, mixed> $data
     * @return self
     */
    public static function create(array $data): self
    {
        return new self($data['bin'], $data['amount'], $data['currency']);
    }
}
