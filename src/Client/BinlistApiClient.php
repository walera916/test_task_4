<?php

namespace App\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Client\ClientInterface;

final class BinlistApiClient implements BinlistClientInterface
{
    private const GET_INFO_URL = 'https://lookup.binlist.net/%s';

    private ClientInterface $apiClient;
    public function __construct()
    {
        $this->apiClient = new Client();
    }


    public function getBinInfo(string $bin): array
    {
        try {
            $response = $this->apiClient->sendRequest(new Request('GET', \sprintf(self::GET_INFO_URL, $bin)));

            return json_decode($response->getBody()->getContents(), true);
        } catch (\Throwable) {
            throw new \DomainException(\sprintf('Invalid response for bin %s', $bin));
        }
    }
}
