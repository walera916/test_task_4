<?php

namespace App\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Client\ClientInterface;

//This API now require API_KEYS, I don't have them, so I have just mock responses
final class ExchangeRateApiClient implements ExchangeRateClientInterface
{
    private const RATE_INFO_URL = 'https://api.exchangeratesapi.io/latest';

    private string $apiKey;

    private ClientInterface $apiClient;
    public function __construct(string $apiKey)
    {
        $this->apiClient = new Client();
        $this->apiKey = $apiKey;
    }

    public function getRates(): array
    {
        try {
            $response = $this->apiClient->sendRequest(new Request('GET', $this->buildUrl(self::RATE_INFO_URL)));

            return json_decode($response->getBody()->getContents(), true);
        } catch (\Throwable) {
            return [];
        }
    }

    private function buildUrl(string $url): string
    {
        return \sprintf('%s?access_key=%s', $url, $this->apiKey);
    }
}
