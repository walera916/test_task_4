<?php

namespace App\Client;

interface ExchangeRateClientInterface
{
    /**
     * @return array<mixed, mixed>
     */
    public function getRates(): array;
}
