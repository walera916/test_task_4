<?php

namespace App\Client;

interface BinlistClientInterface
{
    /**
     * @param string $bin
     * @return array<mixed, mixed>
     */
    public function getBinInfo(string $bin): array;
}
