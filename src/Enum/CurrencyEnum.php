<?php

namespace App\Enum;

enum CurrencyEnum: string
{
    case EUR = 'EUR';
    case USD = 'USD';
    case JPY = 'JPY';
    case GBP = 'GBP';

}
