### How to
1. docker-compose build
2. docker exec -it php-app sh
3. Run script: php app_new.php input.txt
4. Run test: composer tests


#### What can be improved:
1. Use math-php for work with money more correct than floats
2. Use real-life responses in exchangeratesapi.io
3. declare strict types
4. Use strategy instead of CommissionCalculatorManager